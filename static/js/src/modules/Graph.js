import Chart from 'chart.js';
import io from 'socket.io-client';


const
	colors = {
		upload: {rgb: 'rgb(255, 102, 0)', rgba: 'rgba(255, 102, 0, 0.2)'},
		download: {rgb: 'rgb(59, 89, 152)', rgba: 'rgba(59, 89, 152, 0.2)'},
		adownload: {rgb: 'rgb(255, 0, 0)', rgba: 'rgba(255, 0, 0, 0.2)'},
		ping: {rgb: 'rgb(0, 0, 0)', rgba: 'rgba(0, 0, 0, 0.2)'}
	}
;

var
	verticalLinePlugin
;

verticalLinePlugin = {
	afterDatasetsDraw: function(chartInstance) {
		var
			activePoint,
			ctx = chartInstance.ctx,
			x,
			topY,
			bottomY,
			keys
		;

		if (chartInstance.tooltip._active && chartInstance.tooltip._active.length) {
			activePoint = chartInstance.tooltip._active[0];
			x = activePoint.tooltipPosition().x;

			if (Object.keys(chartInstance.scales).indexOf('y-axis-0') > -1) {
				topY = chartInstance.scales['y-axis-0'].top;
				bottomY = chartInstance.scales['y-axis-0'].bottom;
			} else {
				keys = Object.keys(chartInstance.scales);
				keys.splice(keys.indexOf('x-axis-0'), 1);
				topY = chartInstance.scales[keys[0]].top;
				bottomY = chartInstance.scales[keys[0]].bottom;
			}

			ctx.save();
			ctx.beginPath();
			ctx.moveTo(x, topY);
			ctx.lineTo(x, bottomY);
			ctx.lineWidth = 3;
			ctx.strokeStyle = chartInstance.options.defaultColor;
			ctx.stroke();
			ctx.restore();
		}
	}
};

// avarageLinePlugin = {
// 	afterDatasetsDraw: function(chartInstance) {
// 		var
// 			ctx = chartInstance.ctx,
// 			dataSet = chartInstance.data.datasets.filter(data => data.label === 'Download')[0],
// 			sum,
// 			avarage,
// 			position,
// 			text,
// 			textMeasure,
// 			scale = chartInstance.scales['bar-y-axis1']
// 		;
//
// 		if (dataSet.label !== 'Download') {
// 			return;
// 		}
//
// 		sum = dataSet.data.reduce((a, b) => parseInt(a) + parseInt(b));
// 		avarage = sum / dataSet.data.length;
// 		position = avarage / scale.max * 100;
// 		position = position * scale.height / 100;
// 		position = scale.top + scale.height - position;
// 		text = (Math.round(avarage * 100) / 100).toFixed(2) + ' Mbit/s';
// 		textMeasure = ctx.measureText(text).width;
//
// 		ctx.beginPath();
// 		ctx.moveTo(58, position);
// 		ctx.strokeStyle = '#ff0000';
// 		ctx.lineTo(chartInstance.width, position);
// 		ctx.stroke();
//
// 		// draw rectangle and text
// 		ctx.fillStyle = '#ff0000';
// 		ctx.font = '13px Arial';
// 		ctx.fillRect(chartInstance.chartArea.left, position, textMeasure + 10, parseInt(ctx.font) + 4);
// 		ctx.fillStyle = '#ffffff';
// 		ctx.fillText(text, chartInstance.chartArea.left + 5, position + parseInt(ctx.font) - 4);
// 	}
// };


class Component {
	constructor(options) {
		var
			containerID = options.id,
			socket = io(),
			visibilityChange
		;

		if (typeof document.hidden !== 'undefined') { // Opera 12.10 and Firefox 18 and later support
			this._hidden = 'hidden';
			visibilityChange = 'visibilitychange';
		} else if (typeof document.msHidden !== 'undefined') {
			this._hidden = 'msHidden';
			visibilityChange = 'msvisibilitychange';
		} else if (typeof document.webkitHidden !== 'undefined') {
			this._hidden = 'webkitHidden';
			visibilityChange = 'webkitvisibilitychange';
		}

		if (typeof document.addEventListener !== 'undefined' && typeof document.hidden !== 'undefined') {
			document.addEventListener(visibilityChange, this._handleVisibilityChange.bind(this), false);
		}

		socket.on('newDataAvailable', data => {
			var chart = this._chart;

			chart.data.labels.push(data.date);
			chart.data.datasets.forEach(dataset => {
				switch (dataset.label) {
					case 'Download':
						dataset.data.push(data.down);
						break;
					case 'Upload':
						dataset.data.push(data.up);
						break;
					case 'Ping':
						dataset.data.push(data.ping);
						break;
				}
			});
			chart.update();

			if (this._focused === false) {
				document.title = '📣 ' + document.title.replace('📣 ', '');
			}
		});

		this._render(document.getElementById(containerID));
	}

	_handleVisibilityChange() {
		if (document[this._hidden]) {
			this._focused = false;
		} else {
			document.title = document.title.replace('📣 ', '');
			this._focused = true;
		}
	}

	_getDataSet(label, data) {
		let dataSet = {
			type: 'line',
			fill: false,
			label: label,
			data: data,
			backgroundColor: colors[label.toLowerCase()].rgba,
			borderColor: colors[label.toLowerCase()].rgb,
			borderWidth: 1,
			pointBorderWidth: 0,
			pointRadius: 2,
			pointBackgroundColor: colors[label.toLowerCase()].rgb
		};

		if (label === 'Download' || label === 'Upload') {
			dataSet.type = 'bar';
		}

		if (label === 'adownload') {
			dataSet.borderWidth = 3;
			dataSet.pointRadius = 0;
		}

		return dataSet;
	}

	_render(element) {
		var
			ctx = element.querySelector('canvas').getContext('2d'),
			labels = [],
			dataSets = [],
			up = window.graph.data.map(data => data.up),
			down = window.graph.data.map(data => data.down),
			ping = window.graph.data.map(data => {
				if (data.ping > 100) {
					return null;
				}

				return data.ping;
			}),
			avarage = [],
			currentIndex = 0,
			arrayLength = down.length
		;

		labels = window.graph.data.map(data => data.date + ' Uhr');

		dataSets.push(this._getDataSet('Upload', up));
		dataSets.push(this._getDataSet('Download', down));
		dataSets.push(this._getDataSet('Ping', ping));

		while (currentIndex < arrayLength) {
			let
				avarageDownload = down.slice(Math.max(currentIndex - 24, 0), currentIndex + 1),
				result = avarageDownload.reduce((acc, val) => parseFloat(acc) + parseFloat(val)) / avarageDownload.length
			;

			avarage.push(result.toFixed(2));
			currentIndex++;
		}
		dataSets.push(this._getDataSet('adownload', avarage));

		this._chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: labels,
				datasets: dataSets
			},
			options: {
				spanGaps: true,
				responsive: true,
				tooltips: {
					mode: 'index',
					intersect: false
				},
				animation: {
					easing: 'easeOutQuint'
				},
				legend: {
					display: true
				},
				title: {
					display: true,
					text: 'Entwicklung'
				},
				scales: {
					yAxes: [{
						id: 'bar-y-axis1',
						ticks: {
							beginAtZero: true
						}
					}],
					xAxes: [{
						id: 'bar-x-axis1',
						stacked: true,
						ticks: {
							stepSize: 5,
							// minRotation: 65,
							maxTicksLimit: 25
						}
					}]
				}
			},
			plugins: [
				verticalLinePlugin
			]
		});
	}
}

export default Component;
