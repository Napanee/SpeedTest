/* eslint no-console: "off" */
const
	path = require('path'),
	express = require('express'),
	app = express(),
	http = require('http'),
	server = http.createServer(app),
	io = require('socket.io')(server),
	routes = require(path.join(__dirname, 'app', 'routes.js')),
	bodyParser = require('body-parser')
;


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'static')));
app.use(bodyParser.json());
app.on('speedtest:done', data => {
	io.emit('newDataAvailable', data);
});

routes(app);

io.on('connection', client => { // eslint-disable-line no-unused-vars
	console.log('Client connected...');
});

server.listen(3000);
