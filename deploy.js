const
	exec = require('ssh-exec')
;

exec(
	'cd /volume1/web/speedtest;' +
	'git pull;' +
	'npm install;' +
	'npm run prod;' +
	'killall node;' +
	'node server.js;',

	'admin@192.168.2.200'
).pipe(process.stdout);
