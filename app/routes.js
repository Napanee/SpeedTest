const
	moment = require('moment'),
	Client = require('mariasql')
;

var
	c
;


if (process.env && process.env.HOME && process.env.HOME.indexOf('/Users/napanee') === 0) {
	c = new Client({
		host: '127.0.0.1',
		user: 'root',
		password: 'root',
		db: 'speedtest'
	});
} else {
	c = new Client({
		host: 'db',
		user: 'root',
		password: '97do*QMuMvMK@+Q6.A2{{;b42',
		db: 'speedtest'
	});
}

function getInsertedData(insertId) {
	return new Promise((resolve, reject) => {
		var
			data
		;

		c.query('SELECT *, UNIX_TIMESTAMP(timestamp) as timestamp FROM speed WHERE id = :insertId', {insertId: insertId}, (err, rows) => {
			if (err) {
				return reject(err);
			}

			data = {
				date: moment(rows[0].timestamp * 1000).format('DD. MMM YYYY, HH:mm'),
				ping: rows[0].ping,
				down: rows[0].download,
				up: rows[0].upload
			};

			c.end();

			resolve(data);
		});
	});
}

function getData(from, to) {
	var
		graph = {data: []}
	;

	from = moment(from).unix();
	to = moment(to).unix();

	return new Promise((resolve, reject) => {
		var
			output
		;

		c.query('SELECT *, UNIX_TIMESTAMP(timestamp) as timestamp FROM speed WHERE timestamp BETWEEN FROM_UNIXTIME(:from) AND FROM_UNIXTIME(:to)', {from: from, to: to}, (err, rows) => {
			if (err) {
				return reject(err);
			}

			rows.forEach(data => {
				output = {
					date: moment(data.timestamp * 1000).format('DD. MMM YYYY, HH:mm'),
					ping: data.ping,
					down: data.download,
					up: data.upload
				};

				graph.data.push(output);
			});

			c.end();

			resolve(graph);
		});
	});
}

module.exports = function (app) {
	app.get('/', (req, res) => {
		getData(moment().subtract(2, 'weeks').startOf('day'), moment().endOf('day'))
			.then(graph => {
				res.render('index', {
					graph: JSON.stringify(graph),
					fullUrl: req.protocol + '://' + req.get('host') + req.originalUrl
				});
			});
	});

	app.post('/', function (req, res) {
		res.send('{"success": true}');
		if (req.body.success) {
			getInsertedData(req.body.insertId)
				.then(data => {
					app.emit('speedtest:done', data);
				});
		}
	});
};
