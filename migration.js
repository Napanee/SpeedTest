const
	fs = require('fs'),
	path = require('path'),
	Client = require('mariasql')
;

var
	c
;


c = new Client({
	host: '127.0.0.1',
	user: 'root',
	password: '97do*QMuMvMK@+Q6.A2{{;b42'
});

c.query('CREATE DATABASE IF NOT EXISTS speedtest');
c.query('use speedtest');
c.query('CREATE TABLE IF NOT EXISTS speed (' +
			'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,' +
			'download DOUBLE(5,2) UNSIGNED NOT NULL DEFAULT 0,' +
			'upload DOUBLE(4,2) UNSIGNED NOT NULL DEFAULT 0,' +
			'ping DOUBLE(5,2) UNSIGNED NOT NULL DEFAULT 0,' +
			'timestamp TIMESTAMP' +
		') engine=InnoDB default charset latin1');

function insertIntoDB(data) {
	c.query('INSERT INTO speed (download, upload, ping, timestamp) VALUES (:download, :upload, :ping, FROM_UNIXTIME(:timestamp))', {download: data.speeds.download, upload: data.speeds.upload, ping: data.server.ping, timestamp: data.timestamp}, err => {
		if (err) {
			throw err;
		}
	});
}

function readDir(dir) {
	var files = fs.readdirSync(dir);

	files.forEach(file => {
		var
			filePath = path.resolve(dir, file),
			fileStats = fs.lstatSync(filePath),
			output = {},
			timestamp,
			baseName = '',
			lines,
			ping,
			down,
			up
		;

		if (fileStats.isDirectory()) {
			readDir(filePath);
			return;
		}

		timestamp = path.dirname(filePath).split('/').pop();
		baseName = path.basename(filePath, '.log');

		if (baseName === 'output') {
			lines = fs.readFileSync(filePath, 'utf-8').split('\n').filter(value => value !== '');

			if (lines.length === 3) {
				ping = /^Ping:\s([\d.]*)\sms$/g.exec(lines[0]);
				down = /^Download:\s([\d.]*)\sMbit\/s$/g.exec(lines[1]);
				up = /^Upload:\s([\d.]*)\sMbit\/s$/g.exec(lines[2]);

				output = {
					timestamp: timestamp,
					speeds: {
						download: down[1],
						upload: up[1]
					},
					server: {
						ping: ping[1]
					}
				};

				insertIntoDB(output);
			}
		}
	});
}

if (process.env.HOME.indexOf('/var/services/homes/') === 0) {
	readDir('/volume1/web/speedtest/speedlogs');
} else {
	readDir('./CronLogs/synoscheduler/4');
}
