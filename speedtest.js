/* eslint no-console: "off" */
const
	speedTest = require('speedtest-net'),
	Client = require('mariasql'),
	http = require('http'),
	results = []
;

var
	c
;


if (process.env && process.env.HOME && process.env.HOME.indexOf('/Users/napanee') === 0) {
	c = new Client({
		host: '127.0.0.1',
		user: 'root',
		password: 'root',
		db: 'speedtest'
	});
} else {
	c = new Client({
		host: 'db',
		user: 'root',
		password: '97do*QMuMvMK@+Q6.A2{{;b42',
		db: 'speedtest'
	});
}

function _onDataInserted(insertId) {
	return new Promise((resolve, reject) => {
		var
			options = {
				hostname: 'localhost',
				port: 3000,
				path: '/',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				}
			},
			req,
			postData = {
				success: true,
				insertId: insertId
			}
		;

		req = http.request(options, function(res) {
			res.setEncoding('utf8');
			res.on('data', function (body) {
				console.log('Body: ' + body);
			});
			res.on('end', function() {
				resolve();
			});
		});

		req.on('error', function(e) {
			console.log('problem with request: ' + e.message);
			reject();
		});

		req.write(JSON.stringify(postData));
		req.end();
	});
}

function speedtest() {
	speedTest({maxTime: 5000, serverId: 17137})
		.on('data', data => {
			results.push({download: data.speeds.download, upload: data.speeds.upload, ping: data.server.ping});
		})
		.on('done', () => {
			if (results.length < 3) {
				speedtest();
			} else {
				const
					result = results.sort((a, b) => b.download - a.download)[0]
				;

				c.query('CREATE DATABASE IF NOT EXISTS speedtest');
				c.query('CREATE TABLE IF NOT EXISTS speed (' +
							'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,' +
							'download DOUBLE(5,2) UNSIGNED NOT NULL DEFAULT 0,' +
							'upload DOUBLE(4,2) UNSIGNED NOT NULL DEFAULT 0,' +
							'ping DOUBLE(4,2) UNSIGNED NOT NULL DEFAULT 0,' +
							'timestamp TIMESTAMP' +
						') engine=InnoDB default charset latin1');
				c.query('INSERT INTO speed (download, upload, ping) VALUES (:download, :upload, :ping)', {download: result.download, upload: result.upload, ping: Math.min(result.ping, 100)}, (err, rows) => {
					if (err) {
						throw err;
					}

					c.end();

					_onDataInserted(rows.info.insertId).then(() => {
						process.exit();
					});
				});
			}
		})
	;
}

speedtest();
