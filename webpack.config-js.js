module.exports = env => {
	return {
		context: __dirname + '/static/js/src',
		entry: {
			'build': './app.js'
		},
		devtool: env.NODE_ENV === 'production' ? 'source-map' : 'inline-source-map',
		watch: env.NODE_ENV === 'production' ? false : true,
		output: {
			path: __dirname + '/static/js/build',
			filename: '[name].js'
		},
		module: {
			rules: [
				{
					enforce: 'pre',
					test: /\.js$/,
					exclude: /node_modules/,
					loader: 'eslint-loader',
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env']
							}
						}
					]
				}
			]
		}
	};
};
